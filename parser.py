import argparse
import os

import tqdm as tqdm


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--path', action='store', dest='path', required=True,
                        help="Path to file or folder to read log from")
    parser.add_argument('--term', action='store', dest='term', required=True, help="Word to search for")
    return parser.parse_args()

def main():
    options = parse_args()
    files_to_parse = []
    if os.path.isfile(options.path):
        files_to_parse = [os.path.abspath(options.path)]
    elif os.path.isdir(options.path):
        files_to_parse = (os.path.join(options.path, file) for file in os.listdir(options.path) if os.path.isfile(os.path.join(options.path, file)))
    for file in files_to_parse:
        with open(os.path.join(options.path, "result.txt"), 'w') as result:

            with open(file, 'r') as f:
                for line in tqdm.tqdm(f.readlines()):
                    if options.term in line:
                        result.writelines(line)



if __name__ == "__main__":
    main()