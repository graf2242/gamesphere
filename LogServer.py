import random
import psutil
from time import sleep, time
import logging
from logging.handlers import RotatingFileHandler
import argparse

FORMAT = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

my_handler = RotatingFileHandler("MetricServer.log", mode='a', maxBytes=5 * 1024 * 1024,
                                 backupCount=50, encoding=None, delay=0)
my_handler.setFormatter(FORMAT)
my_handler.setLevel(logging.INFO)

loggers_names = ["cpu", 'mem', 'battery']
loggers = {}
for logger_name in loggers_names:
    logger = logging.getLogger(logger_name)
    rand = random.random()
    logger.setLevel(logging.INFO)
    logger.addHandler(my_handler)

    loggers[logger_name] = logger


# Decorate function with metric.
def process_request(t):
    """A dummy function that takes some time."""
    metrics = {
        "proc_percent": psutil.cpu_percent(),
        "frequency": psutil.cpu_freq().current,
        "battery_percent": psutil.sensors_battery().percent if psutil.sensors_battery() else 0,
        "free_mem": psutil.virtual_memory().free,
        "used_mem": psutil.virtual_memory().used,
    }

    def rand_log(logger, text):
        rand = random.random()
        if rand < 0.5:
            loggers[logger].info(text)
        elif rand < 0.9:
            loggers[logger].warning(text)
        else:
            loggers[logger].error(text)



    rand_log("battery", "Setting metric {} to {}".format("proc_percent", metrics["proc_percent"]))
    rand_log("cpu", "Setting metric {} to {}".format("proc_percent", metrics["proc_percent"]))
    rand_log("cpu", "Setting metric {} to {}".format("frequency", metrics["frequency"]))
    rand_log("mem", "Setting metric {} to {}".format("free_mem", metrics["free_mem"]))
    rand_log("mem", "Setting metric {} to {}".format("used_mem", metrics["used_mem"]))
    sleep(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--timeout', type=float, action='store', dest='timeout', required=False, default=1200, help="time to exec")
    options = parser.parse_args()

    start_time = time()
    # Generate some requests.
    while time() - start_time < options.timeout:
        process_request(random.random())
